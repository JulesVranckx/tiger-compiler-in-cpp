#include "ast_evaluator.hh"
#include "../utils/errors.hh"

namespace ast {


int32_t ASTEvaluator::visit(const class IntegerLiteral & node) {
    return node.value ;
}
int32_t ASTEvaluator::visit(const class StringLiteral & node) {
    utils::error("Evaluator not made for StringLiteral") ;
}
int32_t ASTEvaluator::visit(const class BinaryOperator & node) {
    // Operator to be switched
    const auto op = node.op ;
    const auto op_symbol = operator_name[op] ;
    // Left operand is evaluated
    u_int32_t leftOperand = node.get_left().accept(*this) ;
    // Right operand is evaluated
    uint32_t rightOperand = node.get_right().accept(*this) ;
    if (op_symbol == "+") return leftOperand + rightOperand ;
    else if (op_symbol == "-") return leftOperand - rightOperand ;
    else if (op_symbol == "*") return leftOperand * rightOperand ;
    else if (op_symbol == "/") {
            if (rightOperand != 0) {
                return leftOperand / rightOperand ;
            }
            else {
                utils::error("Division by 0") ;
            }
    }
    else if (op_symbol == "<") return leftOperand < rightOperand ;
    else if (op_symbol == "<=") return leftOperand <= rightOperand ;
    else if (op_symbol == "==") return leftOperand == rightOperand ;
    else if (op_symbol == "!=") return leftOperand != rightOperand ;
    else if (op_symbol == ">=") return leftOperand >= rightOperand ;
    else if (op_symbol == ">") return leftOperand > rightOperand ;
    else utils::error("Unknown operator") ;
}
int32_t ASTEvaluator::visit(const class Sequence & node) {
    const auto exprs = node.get_exprs() ;
    uint32_t ret ;
    
    if (!exprs.empty()) {
        for (auto expr = exprs.cbegin(); expr != exprs.cend(); expr++) {
            ret = (*expr)->accept(*this);
        }
        return ret ;
    } 
    else { 
        utils::error("Empty Sequence") ; 
    }
}
int32_t ASTEvaluator::visit(const class Let & node) {
    utils::error("Evaluator not made for Let") ;
}
int32_t ASTEvaluator::visit(const class Identifier & node) {
    utils::error("Evaluator not made for Identifier") ;
}
int32_t ASTEvaluator::visit(const class IfThenElse & node) {
    uint32_t cond = node.get_condition().accept(*this) ;
    if (cond) {
        return node.get_then_part().accept(*this) ;
    }
    else {
        return node.get_else_part().accept(*this) ;
    }

}
int32_t ASTEvaluator::visit(const class VarDecl & node) {
    utils::error("Evaluator not made for VarDecl") ;
}
int32_t ASTEvaluator::visit(const class FunDecl & node) {
    utils::error("Evaluator not made for FunDecl") ;
}
int32_t ASTEvaluator::visit(const class FunCall & node) {
    utils::error("Evaluator not made for FunCall") ;
}
int32_t ASTEvaluator::visit(const class WhileLoop & node) {
    utils::error("Evaluator not made for WhileLoop") ;
}
int32_t ASTEvaluator::visit(const class ForLoop & node) {
    utils::error("Evaluator not made for ForLoop") ;
}
int32_t ASTEvaluator::visit(const class Break & node) {
    utils::error("Evaluator not made for Break") ;
}
int32_t ASTEvaluator::visit(const class Assign & node) {
    utils::error("Evaluator not made for Assign") ;
}

} // namespace ast
