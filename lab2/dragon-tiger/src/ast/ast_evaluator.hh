#ifndef AST_EVALUATOR_HH
#define AST_EVALUATOR_HH

#include <ostream>

#include "nodes.hh"

namespace ast {

class ASTEvaluator : public ConstASTIntVisitor {
public: 
  int32_t visit(const class IntegerLiteral &)  ;
  int32_t visit(const class StringLiteral &) ;
  int32_t visit(const class BinaryOperator &) ;
  int32_t visit(const class Sequence &) ;
  int32_t visit(const class Let &) ;
  int32_t visit(const class Identifier &) ;
  int32_t visit(const class IfThenElse &) ;
  int32_t visit(const class VarDecl &) ;
  int32_t visit(const class FunDecl &) ;
  int32_t visit(const class FunCall &) ;
  int32_t visit(const class WhileLoop &) ;
  int32_t visit(const class ForLoop &) ;
  int32_t visit(const class Break &) ;
  int32_t visit(const class Assign &) ;
};

} // namespace ast

#endif // _AST_EVALUATOR_HH