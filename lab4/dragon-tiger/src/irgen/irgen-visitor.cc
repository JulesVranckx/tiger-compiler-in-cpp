#include <cstdlib>  // For exit
#include <iostream> // For std::cerr
#include "irgen.hh"

#include "llvm/Support/raw_ostream.h"

namespace {

} // namespace

namespace irgen {

llvm::Value *IRGenerator::visit(const IntegerLiteral &literal) {
  return Builder.getInt32(literal.value);
}

llvm::Value *IRGenerator::visit(const StringLiteral &literal) {
  return Builder.CreateGlobalStringPtr(literal.value.get());
}

llvm::Value *IRGenerator::visit(const Break &b) {

  auto try_loop = b.get_loop() ;
  if (try_loop) {
    const Loop * loop = &(try_loop.value()) ; 
    Builder.CreateBr(loop_exit_bbs[loop]) ; 
  }

  return nullptr ;
}

llvm::Value *IRGenerator::visit(const BinaryOperator &op) {
  // Void values can be compared for equality only. We directly
  // return 1 or 0 depending on the equality/inequality operator.
  if (op.get_left().get_type() == t_void) {
    return Builder.getInt32(op.op == o_eq);
  }

  llvm::Value *l = op.get_left().accept(*this);
  llvm::Value *r = op.get_right().accept(*this);

  if (op.get_left().get_type() == t_string) {
    auto const strcmp = Mod->getOrInsertFunction("__strcmp", Builder.getInt32Ty(),
        Builder.getInt8PtrTy(), Builder.getInt8PtrTy()
#if LLVM_VERSION_MAJOR < 5
        , nullptr  // Older versions of LLVM uses C-vararg with a NULL end sigil
#endif // LLVM_MAJOR_VERSION < 5
        );
    l = Builder.CreateCall(strcmp, {l, r});
    r = Builder.getInt32(0);
  }

  switch(op.op) {
    case o_plus: return Builder.CreateBinOp(llvm::Instruction::Add, l, r);
    case o_minus: return Builder.CreateBinOp(llvm::Instruction::Sub, l, r);
    case o_times: return Builder.CreateBinOp(llvm::Instruction::Mul, l, r);
    case o_divide: return Builder.CreateBinOp(llvm::Instruction::SDiv, l, r);
    default: break;
  }

  // Comparisons return an i1 result which needs to be
  // casted to i32, as Tiger might use that as an integer.
  llvm::Value *cmp;

  switch(op.op) {
    case o_eq: cmp = Builder.CreateICmpEQ(l, r); break;
    case o_neq: cmp = Builder.CreateICmpNE(l, r); break;
    case o_gt: cmp = Builder.CreateICmpSGT(l, r); break;
    case o_lt: cmp = Builder.CreateICmpSLT(l, r); break;
    case o_ge: cmp = Builder.CreateICmpSGE(l, r); break;
    case o_le: cmp = Builder.CreateICmpSLE(l, r); break;
    default: assert(false); __builtin_unreachable();
  }

  return Builder.CreateIntCast(cmp, Builder.getInt32Ty(), false);
}

llvm::Value *IRGenerator::visit(const Sequence &seq) {
  llvm::Value *result = nullptr;
  for (auto expr : seq.get_exprs())
    result = expr->accept(*this);
  // An empty sequence should return () but the result
  // will never be used anyway, so nullptr is fine.
  return result;
}

llvm::Value *IRGenerator::visit(const Let &let) {
  for (auto decl : let.get_decls())
    decl->accept(*this);

  return let.get_sequence().accept(*this);
}

llvm::Value *IRGenerator::visit(const Identifier &id) {
  if (id.get_type() == t_void) { 
    return nullptr ;
  }
  llvm::Value * value = address_of(id) ; 
  llvm::Value * ret_value = Builder.CreateLoad(value);
  return ret_value ;
}

llvm::Value *IRGenerator::visit(const IfThenElse &ite) {

  //Allocate in entry
  llvm::Type * type = llvm_type(ite.get_type()) ; 

  //Boolean for type void 
  bool read_write = (ite.get_type() != t_void) ;

  llvm::Value * result ; 
  if(read_write){
    result = alloca_in_entry(type, "result") ;
  }
  else {
    result = nullptr ; 
  }

  //Create three blocs 
  llvm::BasicBlock *  const if_then = llvm::BasicBlock::Create(Context, "if_then", current_function) ;
  llvm::BasicBlock * const if_else = llvm::BasicBlock::Create(Context, "if_else", current_function) ;
  llvm::BasicBlock * const if_end = llvm::BasicBlock::Create(Context, "if_end", current_function) ; 

  llvm::Value * test_condition = ite.get_condition().accept(*this) ; 

  llvm::Value * cond = Builder.CreateICmpNE(test_condition, Builder.getInt32(0), "cond") ;

  Builder.CreateCondBr(cond,if_then, if_else) ;

  // Then Block
  Builder.SetInsertPoint(if_then) ;
  llvm::Value * then_ret = ite.get_then_part().accept(*this) ;
  if (read_write) {
    Builder.CreateStore(then_ret, result) ;
  }
  Builder.CreateBr(if_end) ; 
   

  //Else Block 
  Builder.SetInsertPoint(if_else) ;
  llvm::Value * else_ret = ite.get_else_part().accept(*this) ;
  if (read_write) {
    Builder.CreateStore(else_ret, result) ;
  } 
  Builder.CreateBr(if_end) ; 


  Builder.SetInsertPoint(if_end) ;
  llvm::Value * to_be_ret = nullptr ;
  if(read_write) {
    to_be_ret = Builder.CreateLoad(result) ; 
  }

  return to_be_ret ; 
}

llvm::Value *IRGenerator::visit(const VarDecl &decl) {
  
  llvm::Value * value = generate_vardecl(decl) ;

  auto  expr = decl.get_expr() ; 
  llvm::Value * expr_value = nullptr ;

  if (expr) {
    expr_value = expr.value().accept(*this) ; 
  }

  if (decl.get_type() == t_void) {
    // Void variable, we just accept it and return null
    return nullptr ;
  }

  Builder.CreateStore(expr_value, value) ; 

  return value ; 
}

llvm::Value *IRGenerator::visit(const FunDecl &decl) {
  std::vector<llvm::Type *> param_types;

  if (!decl.is_external) {
    if (decl.get_parent()) {
      const FunDecl * parent = &decl.get_parent().get() ;
      param_types.push_back(frame_type[parent]->getPointerTo()) ;
    }
  }

  for (auto param_decl : decl.get_params()) {
    param_types.push_back(llvm_type(param_decl->get_type()));
  }

  llvm::Type *return_type = llvm_type(decl.get_type());

  llvm::FunctionType *ft =
      llvm::FunctionType::get(return_type, param_types, false);

  llvm::Function::Create(ft,
                         decl.is_external ? llvm::Function::ExternalLinkage
                                          : llvm::Function::InternalLinkage,
                         decl.get_external_name().get(), Mod.get());

  if (decl.get_expr())
    pending_func_bodies.push_front(&decl);

  return nullptr;
}

llvm::Value *IRGenerator::visit(const FunCall &call) {
  // Look up the name in the global module table.
  const FunDecl &decl = call.get_decl().get();
  llvm::Function *callee =
      Mod->getFunction(decl.get_external_name().get());

  if (!callee) {
    // This should only happen for primitives whose Decl is out of the AST
    // and has not yet been handled
    assert(!decl.get_expr());
    decl.accept(*this);
    callee = Mod->getFunction(decl.get_external_name().get());
  }

  std::vector<llvm::Value *> args_values;

  //Escaping variable modification
  if (!decl.is_external) {
    llvm::Value * stat_link ;
    stat_link = frame_up(call.get_depth() - decl.get_depth()).second ;
    args_values.push_back(stat_link) ;

  }
  for (auto expr : call.get_args()) {
    args_values.push_back(expr->accept(*this));
  }

  if (decl.get_type() == t_void) {
    Builder.CreateCall(callee, args_values);
    return nullptr;
  }
  return Builder.CreateCall(callee, args_values, "call");
}

llvm::Value *IRGenerator::visit(const WhileLoop &loop) {
  llvm::BasicBlock * const condition = llvm::BasicBlock::Create(Context, "while_condition", current_function) ;
  llvm::BasicBlock * const body = llvm::BasicBlock::Create(Context, "while_body", current_function) ;
  llvm::BasicBlock * const end = llvm::BasicBlock::Create(Context, "end_while", current_function) ; 

  //Add loop to map, for breaks 
  loop_exit_bbs[&loop] = end ;


  Builder.CreateBr(condition) ; 

  //Condition test
  Builder.SetInsertPoint(condition) ; 
  llvm::Value * test_condition = loop.get_condition().accept(*this) ; 
  llvm::Value * cond = Builder.CreateICmpNE(test_condition, Builder.getInt32(0), "cond") ;
  Builder.CreateCondBr(cond,body, end) ;

  //Body
  Builder.SetInsertPoint(body) ;
  loop.get_body().accept(*this) ;
  Builder.CreateBr(condition) ;

  Builder.SetInsertPoint(end) ;

  return nullptr ;
}

llvm::Value *IRGenerator::visit(const ForLoop &loop) {
  llvm::BasicBlock *const test_block =
      llvm::BasicBlock::Create(Context, "loop_test", current_function);
  llvm::BasicBlock *const body_block =
      llvm::BasicBlock::Create(Context, "loop_body", current_function);
  llvm::BasicBlock *const end_block =
      llvm::BasicBlock::Create(Context, "loop_end", current_function);
  
  
  //Add loop to map, for breaks 
  loop_exit_bbs[&loop] = end_block ;
  
  llvm::Value *const index = loop.get_variable().accept(*this);
  llvm::Value *const high = loop.get_high().accept(*this);
  Builder.CreateBr(test_block);

  Builder.SetInsertPoint(test_block);
  Builder.CreateCondBr(Builder.CreateICmpSLE(Builder.CreateLoad(index), high),
                       body_block, end_block);

  Builder.SetInsertPoint(body_block);
  loop.get_body().accept(*this);
  Builder.CreateStore(
      Builder.CreateAdd(Builder.CreateLoad(index), Builder.getInt32(1)), index);
  Builder.CreateBr(test_block);

  Builder.SetInsertPoint(end_block);
  return nullptr;
}

llvm::Value *IRGenerator::visit(const Assign &assign) {
  
  llvm::Value * right = assign.get_rhs().accept(*this) ;
  if (assign.get_lhs().get_type() == t_void) { 
    return nullptr ;
  }
  llvm::Value * left = address_of(assign.get_lhs())  ;
  

  llvm::Value * ret_value = Builder.CreateStore(right,left) ;

  return ret_value ; 
}

} // namespace irgen
