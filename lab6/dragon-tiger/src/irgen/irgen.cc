#include "irgen.hh"
#include "../utils/errors.hh"

#include "llvm/IR/Verifier.h"
#include "llvm/Support/raw_ostream.h"

using utils::error;

namespace irgen {

IRGenerator::IRGenerator() : Builder(Context) {
  Mod = llvm::make_unique<llvm::Module>("tiger", Context);
}

llvm::Type *IRGenerator::llvm_type(const ast::Type ast_type) {
  switch (ast_type) {
  case t_int:
    return Builder.getInt32Ty();
  case t_string:
    return Builder.getInt8PtrTy();
  case t_void:
    return Builder.getVoidTy();
  default:
    assert(false); __builtin_unreachable();
  }
}

llvm::Value *IRGenerator::alloca_in_entry(llvm::Type *Ty,
                                          const std::string &name) {
  llvm::IRBuilderBase::InsertPoint const saved = Builder.saveIP();
  Builder.SetInsertPoint(&current_function->getEntryBlock());
  llvm::Value *const value = Builder.CreateAlloca(Ty, nullptr, name);
  Builder.restoreIP(saved);
  return value;
}

void IRGenerator::print_ir(std::ostream *ostream) {
  // FIXME: This is inefficient. Should probably take a filename
  // and use directly LLVM raw stream interface
  std::string buffer;
  llvm::raw_string_ostream OS(buffer);
  OS << *Mod;
  OS.flush();
  *ostream << buffer;
}

llvm::Value *IRGenerator::address_of(const Identifier &id) {
  assert(id.get_decl());
  const VarDecl &decl = dynamic_cast<const VarDecl &>(id.get_decl().get());

  if (decl.get_escapes()) {

    std::pair<llvm::StructType * , llvm::Value *> up = frame_up(id.get_depth() - decl.get_depth()) ;
   
    int index = frame_position[&decl] ;
    llvm::Value * ret = Builder.CreateStructGEP(up.first, up.second, index) ;

    return ret ;
  }
  return allocations[&decl];
}

void IRGenerator::generate_program(FunDecl *main) {
  main->accept(*this);

  while (!pending_func_bodies.empty()) {
    generate_function(*pending_func_bodies.back());
    pending_func_bodies.pop_back();
  }
}

void IRGenerator::generate_function(const FunDecl &decl) {
  // Reinitialize common structures.
  allocations.clear();
  loop_exit_bbs.clear();

  // Set current function
  current_function = Mod->getFunction(decl.get_external_name().get());
  current_function_decl = &decl;
  std::vector<VarDecl *> params = decl.get_params();

  // Create a new basic block to insert allocation insertion
  llvm::BasicBlock *bb1 =
      llvm::BasicBlock::Create(Context, "entry", current_function);

  Builder.SetInsertPoint(bb1) ;
  generate_frame() ;

  // Create a second basic block for body insertion
  llvm::BasicBlock *bb2 =
      llvm::BasicBlock::Create(Context, "body", current_function);

  Builder.SetInsertPoint(bb2);

  // Set the name for each argument and register it in the allocations map
  // after storing it in an alloca.
  unsigned i = 0;
  int decalage = 0 ;
  for (auto &arg : current_function->args()) {
    if (i==0 && !decl.is_external && decl.get_parent()) {
        llvm::Value * stored = Builder.CreateStructGEP(frame_type[&decl], frame, 0) ;
        Builder.CreateStore(&arg, stored) ;
        decalage = 1 ;
    }
    else {
      arg.setName(params[i-decalage]->name.get());
      llvm::Value *const shadow = generate_vardecl(*params[i-decalage]) ;
      Builder.CreateStore(&arg, shadow);
      
    }
    i++;
  }

  // Visit the body
  llvm::Value *expr = decl.get_expr()->accept(*this);

  // Finish off the function.
  if (decl.get_type() == t_void)
    Builder.CreateRetVoid();
  else
    Builder.CreateRet(expr);

  // Jump from entry to body
  Builder.SetInsertPoint(bb1);
  Builder.CreateBr(bb2);

  // Validate the generated code, checking for consistency.
  llvm::verifyFunction(*current_function);
}

void IRGenerator::generate_frame() { 

  const FunDecl * decl = current_function_decl ; 

  // Get the escaping variables
  const std::vector<VarDecl *> escape_var = decl->get_escaping_decls() ;

  //Create and fill the vector of the needed types
  std::vector<llvm::Type *> needed_types ; 
  
  //Look for parent function type (if any)
  if (decl->get_parent()) { 
    const FunDecl * parent_decl = &(decl->get_parent().value()) ;
    llvm::Type * parent_func_type =  frame_type[parent_decl]->getPointerTo(); 
    needed_types.push_back(parent_func_type) ; 
  }
  
  llvm::Type * crrt_type ;
  for (VarDecl * decl : escape_var) { 
    //Fill with the non void escaping variables types
    if (decl->get_type() != t_void) { 
      crrt_type = llvm_type(decl->get_type()) ; 
      needed_types.push_back(crrt_type) ;
    }
  }

  //Create a new structure
  llvm::StringRef name("ft_" + decl->name.get()) ;
  llvm::StructType * frame_struct = llvm::StructType::create(Context, needed_types, name) ; 


  //Register the type
  frame_type[decl] = frame_struct ;

  // Create a new object and allocate it on the stack
  llvm::Value * new_frame = alloca_in_entry(frame_struct, "frame_" + decl->get_external_name().get()) ;
  frame = new_frame ;
}

std::pair<llvm::StructType *, llvm::Value *> IRGenerator::frame_up(int levels) { 
  
  //Initial values
  const FunDecl * fun = current_function_decl ;
  llvm::Value * sl = frame ;

  //Go up in the levels
  llvm::Value * temp ;
  llvm::StructType * type ;
  for (int i = 0; i < levels; i++) {
    type = frame_type[fun] ;
    temp = Builder.CreateStructGEP(type, sl, 0) ;
    
    sl = Builder.CreateLoad(temp) ;
    fun = &(fun->get_parent().get()) ;
  }

  std::pair<llvm::StructType *, llvm::Value *> ret(frame_type[fun], sl) ;

  return ret ;
}

llvm::Value *IRGenerator::generate_vardecl(const VarDecl &decl) {

  if (decl.get_type() == t_void) {
    return nullptr ;
  }

  

  // Case the variable does not escape
  if (!decl.get_escapes()) {
    
    llvm::Type * varType = llvm_type(decl.get_type()) ;
    std::string name  = decl.name.get() ;

    llvm::Value * value = alloca_in_entry(varType, name) ; 
    allocations[&decl] = value ;

    return value ;
  }

  // Case the variable escapes

  else  {

    //find the position of the variable
    std::vector<VarDecl *> esc_var = current_function_decl->get_escaping_decls() ;
    int index = 0 ;
    for (uint i = 0; i < esc_var.size(); i++) {
      if (esc_var[i] == &decl) {
        index = i ; 
        break ; 
      } 
    }

    if (current_function_decl->get_parent()) {
      index = index + 1 ;
    }

    frame_position[&decl] = index;


    llvm::Value * add ;
    llvm::Type * type = frame_type[current_function_decl] ;
    add = Builder.CreateStructGEP(type, frame, index) ;
    allocations[&decl] = add ; 

    return add ;
  }

} 

} // namespace irgen
