#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "runtime.h"

__attribute__((noreturn))
static void error(const char *msg) {
  fprintf(stderr, "%s\n", msg);
  exit(EXIT_FAILURE);
}

void __print_err(const char *s) {
  fprintf(stderr, "%s", s) ;
}

void __print(const char *s) {
  printf("%s", s);
}

void __print_int(const int32_t i) {
  printf("%d", i);
}

void __flush(void) {
  fflush(stdout);
}

const char *__getchar(void) {
  static char ret[2] ;
  int check = scanf("%c", ret) ;

  return ret ;
  
}

int32_t __ord(const char *s) {
  if (!strcmp(s,"")) {
    return -1 ;
  }
  else {
    int32_t ret = (unsigned char) *s;
    return ret ;
    
  }
}

const char *__chr(int32_t i) {
  if (!i) {
    return "" ;
  }
  else if (i < 0 || i > 255) {
    exit(EXIT_FAILURE) ;
  }
  else {
    static char  ret[2] ;
    *ret = (char) i ;
    ret[1] = 0 ;
    return ret ;
  }
}

int32_t __size(const char *s) {
  int32_t size = 0 ;
  while (strcmp(s, "\0")) {
    size++ ;
    s++ ;
  }
  return size ;
}

const char *__substring(const char *s, int32_t first, int32_t length) {
  
  //Vérification sur les arguments
  if(!strcmp(s,"")) {
    if (first != 0 || length != 0) {
      __print_err("Impossible substring non null on empty string") ;
      exit(EXIT_FAILURE) ;
    }
    return "" ;
  }

  int32_t siz = strlen(s) ;
  int check = (first + length <= siz) ;
  check = check && (first >= 0 && length >= 0) ;
  if(!check) {
    __print_err("Bad bounds for substring") ;
    exit(EXIT_FAILURE) ;
  }

  
  char * ret = (char *)malloc(length * sizeof(char)) ;
  
  for (int i = first; i < first + length; i++) {
    ret[i - first] = s[i] ;
  }

  return ret ;

}

const char *__concat(const char *s1, const char *s2) {
  int32_t siz1 = strlen(s1) ; 
  int32_t siz2 = strlen(s2) ;
  int32_t tot = siz1 + siz2 ;

  char * ret = (char*)malloc(tot * sizeof(char)) ;
  for (int i = 0 ; i < siz1; i++) {
    ret[i] = s1[i] ;
  }
  for (int i = 0; i < siz2; i++) {
    ret[siz1 + i] = s2[i] ;
  }

  return ret ;
}

int32_t __strcmp(const char *s1, const char *s2) {
  int ret = strcmp(s1, s2) ;

  if(!ret) {
    return 0 ;
  }
  else if (ret < 0) {
    return -1 ;
  }
  else {
    return 1 ;
  }
  
}

int32_t __streq(const char *s1, const char *s2) {
  return __not(abs(strcmp(s1,s2))) & 1 ;
}

int32_t __not(int32_t i) {
  return (i==0) & 1;
}

void __exit(int32_t c) {
  exit(c);
}
