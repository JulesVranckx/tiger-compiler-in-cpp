#include "escaper.hh"
#include "../utils/errors.hh"

namespace ast {
namespace escaper {

void Escaper::visit(IntegerLiteral & integer) { 
    //Nothing
}

void Escaper::visit(StringLiteral & string) {
    //Nothing
}

void Escaper::visit(BinaryOperator & binop) { 
    binop.get_left().accept(*this) ;
    binop.get_right().accept(*this) ;
}

void Escaper::visit(Sequence & seq){
    std::vector<Expr * > exprs = seq.get_exprs() ;
    for (Expr * expr : exprs) {
        expr->accept(*this) ;
    }
 }

void Escaper::visit(Let & let) {
    auto decls = let.get_decls() ; 
    for (Decl * decl : decls) {
        decl->accept(*this) ;
    }
    let.get_sequence().accept(*this) ;
}

void Escaper::visit(Identifier &id) {
    //Nothing
}

void Escaper::visit(IfThenElse & ite){
    ite.get_condition().accept(*this) ;
    ite.get_else_part().accept(*this) ;
    ite.get_then_part().accept(*this) ;
}

void Escaper::visit(VarDecl & varDecl){

    if (varDecl.get_escapes()) {
        crrrt_func->get_escaping_decls().push_back(&varDecl) ;
        std::cout << "Added ! "<< crrrt_func->name.get() << std::endl ;
    }
}

void Escaper::visit(FunDecl & funDecl){
    FunDecl * ex_func = crrrt_func ;
    crrrt_func = &funDecl ;
    

    std::cout << "Current function : " << crrrt_func->name.get() << std::endl ;

    auto params = funDecl.get_params() ;
    for (VarDecl * param : params) {
        param->accept(*this) ;
    }

    auto expr = funDecl.get_expr() ;

    if (expr) {
        expr.value().accept(*this) ;
    }

    crrrt_func = ex_func ; 
    std::cout << "Current function : " << crrrt_func->name.get() << std::endl ;

}

void Escaper::visit(FunCall & funCall){
    auto args = funCall.get_args() ;
    for (Expr * expr : args) {
        expr->accept(*this) ; 
    }
}

void Escaper::visit(WhileLoop & whileLoop){
    whileLoop.get_condition().accept(*this) ;
    whileLoop.get_body().accept(*this) ;    
}

void Escaper::visit(ForLoop & forLoop){
    forLoop.get_variable().accept(*this) ;
    forLoop.get_high().accept(*this) ;
    forLoop.get_body().accept(*this) ;
}

void Escaper::visit(Break & b){
    //Nothing
}

void Escaper::visit(Assign & assign){
    assign.get_lhs().accept(*this) ;
    assign.get_rhs().accept(*this) ;
}

} // namespace escaper
} // namesspace ast