#include "type_checker.hh"
#include "../utils/errors.hh"

namespace ast {
namespace type_checker {

void TypeChecker::visit(IntegerLiteral &integer) {
    integer.set_type(t_int) ;
}

void TypeChecker::visit(StringLiteral &string) {
    string.set_type(t_string) ;
}

void TypeChecker::visit(BinaryOperator &binop) {
    // First we go through the tree
    binop.get_left().accept(*this) ;
    binop.get_right().accept(*this) ;

    Type left = binop.get_left().get_type() ;
    Type right = binop.get_right().get_type() ;

    if (left == t_int && right == t_int) {
        binop.set_type(t_int) ;
    }
    else if (left == t_string && right == t_string) {
        if (binop.op == o_plus || binop.op == o_minus || binop.op == o_times 
        || binop.op == o_divide) {
            utils::error(binop.loc, "Arithmetic operation not supported by strings") ;
        }
        binop.set_type(t_int) ;
    }
    else if (left == t_void && right == t_void) {
        if (binop.op != o_eq) {
            utils::error(binop.loc, "Can only test equality for void variables") ;
        }
        binop.set_type(t_int) ;
    }
    else {
        utils::error(binop.loc, "Must use binary operator with same types") ;
    }
}

void TypeChecker::visit(Sequence & seq) {
    auto exprs = seq.get_exprs() ;

    if (exprs.empty()) {
        //Empty Sequence, we just need to set its type to void
        seq.set_type(t_void) ;
        return ;
    }

    Type type ;
    for (Expr* expr : exprs) {
        expr->accept(*this) ;
        type = expr->get_type() ;
    }
    seq.set_type(type) ;
    
}

void TypeChecker::visit(Let &let) {
    //Visit all the decls
    std::vector<Decl*> decls = let.get_decls() ;
    for (Decl * decl : decls) {
        decl->accept(*this) ;
    }

    //Visit the sequence and get type
    let.get_sequence().accept(*this) ; 
    Type type = let.get_sequence().get_type() ;
    let.set_type(type) ;
}

void TypeChecker::visit(Identifier &id) {
    auto decl = id.get_decl() ;
    if (decl) {
        Type type = decl.value().get_type() ; 
        id.set_type(type) ;
    }
    else {
        utils::error(id.loc, "Can't find declaration") ;
    }
}

void TypeChecker::visit(IfThenElse &ite) {      
    ite.get_condition().accept(*this) ;
    ite.get_then_part().accept(*this) ;
    ite.get_else_part().accept(*this) ;

    //Check if is an int
    Type if_type = ite.get_condition().get_type() ;

    if (if_type != t_int) {
        utils::error(ite.loc, "Condition must be a int") ;
    }

    // Check branches have same type
    Type then_type = ite.get_then_part().get_type() ;
    Type else_type = ite.get_else_part().get_type() ; 

    if (else_type != then_type) {
        utils::error(ite.loc, "Different types in if then else") ;
    }

    ite.set_type(then_type) ;
}

void TypeChecker::visit(VarDecl &decl) {
    
    Type expt_type = t_void;

    if (!decl.type_name) {

        //Variable without explicit type

        auto expr = decl.get_expr() ;

        if( !expr) {
            utils::error(decl.loc, "Function without explicit type must have an expression") ;
        }
        expr.value().accept(*this) ;

        expt_type = expr.value().get_type() ;
        //We set the implicit type of the expression
        decl.set_type(expt_type) ;
        return ;
    }

    //Variable with explicit type

    if (decl.type_name.value().get() == "int") {
        expt_type = t_int ;
    }
    else if (decl.type_name.value().get() == "string") {
        expt_type = t_string ;
    }
    else {
        utils::error(decl.loc, "Type not recognized") ;
    }

    auto expr = decl.get_expr() ;
    if (!expr) {
        //No check neede here, since there is no expression
        decl.set_type(expt_type) ;
    }
    else {
        //Check both type are same
        expr.value().accept(*this) ;
        if (expt_type != expr.value().get_type()) {
            utils::error(decl.loc, "Missmatch variable type !") ;
        }
        decl.set_type(expt_type) ;
    }
}

void TypeChecker::visit(FunDecl &decl) {

    //Has already been visited
    if (decl.get_type() != t_undef) {
        return ;
    }
    
    //Visit parameters
    for (VarDecl * param : decl.get_params()) {
        param->accept(*this) ;
    }
    

    Type expected_type = t_void ; 
    //Implicit type (which is void)
    if (decl.type_name) { 
        
        if (decl.type_name.value().get() == "int") {
            //Int return
            expected_type = t_int ; 
        }
        else if (decl.type_name.value().get() == "string") {
            //String return
            expected_type  =t_string; 
        }
        else {
            utils::error(decl.loc, "Type not recognized") ;
        } 
    }

    decl.set_type(expected_type) ; 

    //Type has been set according to explicit name, now we confirm it was ok
    auto expr = decl.get_expr() ;
    if (expr){
        expr->accept(*this) ;
        if (expected_type != expr.value().get_type()) {
            utils::error(decl.loc, "Declaration type does not match") ;
        }
    }
}

void TypeChecker::visit(FunCall &call) {

    auto decl = call.get_decl() ; 

    if (!decl) {
        utils::error(call.loc, "Declaration not found") ;
    }

    Type decl_type = decl.value().get_type() ;

    if (decl_type == t_undef) {       
        decl.value().accept(*this) ;
        decl_type = decl.value().get_type() ;
    }     

    call.set_type(decl_type) ;

    //Check for the parameters

    auto params = decl.value().get_params() ;

    auto args = call.get_args() ;

    if (params.size() != args.size()) {
        utils::error(call.loc, "Wrong number of arguments") ;
    } 

    VarDecl * param ; 
    Expr * arg ; 

    for (uint i = 0; i < params.size(); i++) {
        param = params.at(i) ;
        arg = args.at(i) ;
        arg->accept(*this) ;
        if (param->get_type() != arg->get_type()) {
            utils::error(call.loc, "Wrong argument type") ;
        }
    }
}

void TypeChecker::visit(WhileLoop &loop) {
    loop.get_condition().accept(*this) ;
    loop.get_body().accept(*this) ;

    //Check condition
    Type cond_type = loop.get_condition().get_type() ;
    if (cond_type != t_int) {
        utils::error(loop.loc, "Condition must be an int") ;
    }

    Type body_type = loop.get_body().get_type() ;

    if (body_type != t_void) {
        utils::error(loop.loc, "Body of while must be void") ;
    }

    loop.set_type(t_void) ;
}

void TypeChecker::visit(ForLoop &loop) {
    loop.get_variable().accept(*this) ;
    loop.get_high().accept(*this) ;
    loop.get_body().accept(*this) ;

    if (loop.get_variable().get_type() != t_int || 
        loop.get_high().get_type() != t_int) {
            utils::error(loop.loc, "ForLoop bounds must be integers") ;
    }

    if (loop.get_body().get_type() != t_void) {
        utils::error(loop.loc, "ForLoop body must be void") ;
    }

    loop.set_type(t_void) ;
}

void TypeChecker::visit(Break &b) {
    b.set_type(t_void) ;
}

void TypeChecker::visit(Assign &assign) {
    assign.get_lhs().accept(*this) ;
    assign.get_rhs().accept(*this) ;

    if (assign.get_lhs().get_type() != assign.get_rhs().get_type()) {
        utils::error(assign.loc, "Assign different types") ;
    }

    assign.set_type(t_void) ;
}

} //namespace type_checker
} //namespace ast